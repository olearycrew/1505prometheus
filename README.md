## About 

Configuration for Prometheus running on the Mac Mini

See https://gitlab.com/olearycrew/1505network/blob/master/Services.md

## Installation
* Undocumented.  Sorry future self.

## Deploy
* `ssh brendan@10.0.0.33`
* `cd /etc/prometheus`
* `sudo git pull`
* `sudo systemctl restart prometheus`

## Loki (WIP)

### Installation

* `sudo curl -fSL -o "/usr/local/bin/loki.gz" "https://github.com/grafana/loki/releases/download/v0.4.0/loki-linux-amd64.gz"`
* `sudo gunzip "/usr/local/bin/loki.gz"`
* `sudo chmod a+x "/usr/local/bin/loki"`
* `sudo ln -s /etc/prometheus/loki/ /etc/loki`

### Install Promtail

* `sudo curl -fSL -o "/usr/local/bin/promtail.gz" "https://github.com/grafana/loki/releases/download/v0.4.0/promtail-linux-amd64.gz"`
* `sudo gunzip "/usr/local/bin/promtail.gz"`
* `sudo chmod a+x "/usr/local/bin/promtail"`